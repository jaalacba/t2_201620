package mundo;

import java.util.Date;

public class Clientes extends Trabajo {

	private String negocio;
	
	public Clientes(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TipoEventoTrabajo pTipo, String pNegocio) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		negocio = pNegocio;

	}

	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con SOCIOS: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipo
	  		+"\nEmpresa: " + this.negocio
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}

}
