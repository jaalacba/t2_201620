package mundo;

import java.util.Date;

public class Trabajo extends Evento
{

	
	public enum  TipoEventoTrabajo {
		JUNTAS,
		ALMUERZOS,
		CENAS,
		COCTELES,
		PRESENTACIONES,
		REUNIONES,
	}
	
	protected TipoEventoTrabajo tipo;
	
	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TipoEventoTrabajo pTipo) {
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo = pTipo;

	}

}
